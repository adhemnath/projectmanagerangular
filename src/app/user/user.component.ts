import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskserviceService } from '../services/taskservice.service';
import { userModel } from '../models/userModel';
import { DatePipe } from '@angular/common';
import { error } from 'protractor';
import { FilterPipe } from '../SearchPipe';
import * as $ from 'jquery';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [DatePipe]
})
export class UserComponent implements OnInit {
  taskID:string;
  userDetail:userModel;
  allUserList:userModel[]; 
  mode:string="Add";
  disabled:boolean=false;
  searchString='';
  constructor(private taskService:TaskserviceService, private route:ActivatedRoute,private router:Router,private datePipe: DatePipe) { }

  ngOnInit() {
    this.userDetail=new userModel();
    this.allUserList = [];
    this.getAlluser();

  }

  getAlluser()
  {
    this.taskService.getAllUsers().subscribe(
      getuser=>{        
        this.allUserList = getuser;
      })
         
  }

  onSubmit()
  {        
    if(this.validatesubmit())
    {
    if(this.mode=="Edit")
    {
      this.taskService.updateUser(this.userDetail)
      .subscribe(result=>{
        this.getAlluser();
      })
      this.onClear()
    }
    else{
      this.taskService.adduser(this.userDetail)
      .subscribe(result=>{
        this.getAlluser();
      })
      this.onClear()
    }
  }
  else
  {
    alert('Please populate all fields');
  }
  }

  sortFirst_Name()
  {
    this.allUserList.sort(function(a, b){
      var nameA=a.First_Name.toLowerCase(), nameB=b.First_Name.toLowerCase()
      if (nameA < nameB) //sort string ascending
          return -1 
      if (nameA > nameB)
          return 1
      return 0 //default return value (no sorting)
  })
  
  }

  sortLast_Name()
  {
    this.allUserList.sort(function(a, b){
      var nameA=a.Last_Name.toLowerCase(), nameB=b.Last_Name.toLowerCase()
      if (nameA < nameB) //sort string ascending
          return -1 
      if (nameA > nameB)
          return 1
      return 0 //default return value (no sorting)
  })
  
  }


  sortEmpid()
  {
    this.allUserList.sort(function(a, b){
      var nameA=a.Employee_ID, nameB=b.Employee_ID
      if (nameA < nameB) //sort string ascending
          return -1 
      if (nameA > nameB)
          return 1
      return 0 //default return value (no sorting)
  })
  
  }

  editUser(user:userModel)
  {
    this.userDetail.First_Name = user.First_Name;
    this.userDetail.Last_Name = user.Last_Name;
    this.userDetail.Employee_ID = user.Employee_ID;
    this.userDetail.User_ID = user.User_ID;
    this.mode='Edit';
    if(this.validatesubmit())
    {
      document.getElementById("submitBtn").innerHTML =  'Edit';
      this.taskService.updateUser(this.userDetail)
      .subscribe(result=>{
      },
      error=>{},
      ()=>{ this.getAlluser();})
    }
    else{
      alert('Please populate all fields');
    }
  }

  deleteUser(user:userModel)
  {
    this.taskService.deleteuser(user.User_ID).subscribe(A=>{
    },
    error=>{},
    ()=>{
      this.getAlluser();
    });
  }

  onClear()
  {   
      this.userDetail=new userModel();   
      document.getElementById("submitBtn").innerHTML =  'Add';
      this.mode='Add';
  }

  validatesubmit():boolean
  {
    if(this.userDetail.First_Name && this.userDetail.Last_Name && this.userDetail.Employee_ID)
    {
      return true;
    }

    return false;
  }

}
