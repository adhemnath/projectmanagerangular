export class AppConstants{

    constructor()
    {

    }

    //public baseUrl:string="http://localhost:51623/";
    public baseUrl:string="http://localhost/ProjectManager.API/";    
    public getTasks:string="GetAllTasks";
    public getTaskByID:string="GetTaskByID";
    public addTask:string="PostTask";
    public updateTask:string="UpdateTask";  
    
    public addUser:string="PostUser";
    public updateUser:string="UpdateUser";
    public getAllUsers:string = 'GetAllUsers'  
    public deleteUser:string = 'DeleteUser'

    public postProject:string = 'PostProject'
    public getAllProjectDetails:string='GetAllProjectDetails'
    public updateProject:string ="UpdateProject"
    public deleteProject:string = "DeleteProject"

    public getAllParentTasks:string = 'GetAllParentTasks'
    

    

}