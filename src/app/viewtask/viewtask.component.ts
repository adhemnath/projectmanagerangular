import { Component, OnInit } from '@angular/core';
import { taskModel } from '../models/taskmodel';
import { TaskserviceService } from '../services/taskservice.service';
import { DatePipe } from '@angular/common';
import { projectModel } from '../models/projectModel';
import {parentTaskModel} from '../models/parentTaskModel'
import * as $ from 'jquery';

@Component({
  selector: 'app-viewtask',
  templateUrl: './viewtask.component.html',
  styleUrls: ['./viewtask.component.css'],
  providers: [DatePipe]
})
export class ViewtaskComponent implements OnInit {
   
  allTaskItems:taskModel[]=[];
  filterAllTaskItems:taskModel[]=[];
  allProjectList :projectModel[]=[];
  selectedproject:projectModel

  getProject:projectModel;
  taskItems:any;
  addTask:any;
  viewTask:any;
  getparentTask:parentTaskModel;
  allParenttask :parentTaskModel[]=[];
  searchString:string;

  constructor(private taskService:TaskserviceService,private datePipe: DatePipe ) { }

  ngOnInit() {   
    this.selectedproject = new projectModel(); 
    this.getProject =   new projectModel();      
    this.addTask= this.setaddTask();
    this.viewTask= this.setviewTask();
    this.addTask.innerHTML="Add Task";
    //this.viewTask.innerHTML="View Task"; 
    this.getAllTasks();
    this.getAllProjectDetails();

  }

  setaddTask():any
  {   
    return document.getElementById("updateAnchor");
  }
  setviewTask():any
  {
    return  document.getElementById("viewAnchor");
  }

  getAllProjectDetails()
  {   

    this.taskService.getAllProjectDetails().subscribe(
      getProject=>{        
        this.allProjectList = getProject;
      })
  }

  getAllTasks()
  {
    this.allTaskItems=[];
    this.filterAllTaskItems=[];
    this.taskService.getAllTasks()
    .subscribe(result=>{
      this.taskItems=result;
      this.taskItems.forEach(taskModelValue=>{
        let gettaskModel = new taskModel();
        gettaskModel.Priority = taskModelValue.Priority;
        gettaskModel.EndDate = taskModelValue.EndDate;
        gettaskModel.StartDate = taskModelValue.StartDate;
        gettaskModel.Task_ID = taskModelValue.Task_ID;
        gettaskModel.Parent_ID = taskModelValue.Parent_ID;
        gettaskModel.TaskName = taskModelValue.TaskName;
        gettaskModel.Mode = taskModelValue.Mode;
        gettaskModel.Project_ID = taskModelValue.Project_ID;
        gettaskModel.Project_Name = taskModelValue.Project_Name;
       
        if(this.allProjectList.filter(A=>A.Project_ID==taskModelValue.Project_ID))
        {
         this.selectedproject = this.allProjectList.filter(A=>A.Project_ID==taskModelValue.Project_ID)[0];
        }        
        if(this.selectedproject)
        {
        gettaskModel.Project_Name =  this.selectedproject.Project_Name;
        }
        
        if(taskModelValue.Status=="Y")
        {
          gettaskModel.DisabledMode=true;
        }
        else
        {
          gettaskModel.DisabledMode=false;
        }

        this.allTaskItems.push(gettaskModel);
        
        this.filterAllTaskItems.push(gettaskModel);


      })
      this.getAllParentTask();
    })
  }

  EndTask(value:any)
  {   
    value.Status="Y";    
    this.taskService.updateTask(value)
    .subscribe(result=>{
      this.getAllTasks();
    })
  }

  
  selectproject(project:projectModel,userModal:any)
  {
        
    this.getProject = project;

    let filteredTaskItems:taskModel[]=this.allTaskItems;
   
    if(project.Project_Name)
    {
      filteredTaskItems=filteredTaskItems.filter(A=>A.Project_ID==project.Project_ID.toString()) ;
    } 
    
    this.filterAllTaskItems=filteredTaskItems;
        
  }

  getdateValue(value:any):string{

    let pipedValue=this.datePipe.transform(value, 'dd/MM/yyyy');
     return pipedValue;
  }

  sortByDate():any
{
  this.filterAllTaskItems.sort(function(a, b){
    let dateA:any=new Date(a.StartDate) 
    let dateB:any=new Date(b.StartDate)
    return dateA-dateB //sort by date ascending
})
}

sortByEnddate():any
{
  this.filterAllTaskItems.sort(function(a, b){
    let dateA:any=new Date(a.EndDate) 
    let dateB:any=new Date(b.EndDate)
    return dateA-dateB //sort by date ascending
})
}

sortByPriority():any
{
  this.filterAllTaskItems.sort(function(a, b){
    var nameA=a.Priority, nameB=b.Priority
    if (nameA < nameB) //sort string ascending
        return -1 
    if (nameA > nameB)
        return 1
    return 0 //default return value (no sorting)

})
}

sortByCompleted():any
{
  this.filterAllTaskItems.sort(function(a, b){
    var nameA=a.Status, nameB=b.Status
    if (nameA < nameB) //sort string ascending
        return -1 
    if (nameA > nameB)
        return 1
    return 0 //default return value (no sorting)

})
}

getAllParentTask()
  {   

    this.taskService.getAllParentTasks().subscribe(
      getParenttask=>{        
        this.allParenttask = getParenttask;
        this.filterAllTaskItems.forEach(taskitems=>{
          if(this.allParenttask.some(A=>A.Parent_ID ==taskitems.Parent_ID))
          {
            taskitems.Parent_Task =this.allParenttask.filter(A=>A.Parent_ID ==taskitems.Parent_ID)[0].Parent_Task;
          }

        });
      })
  }


}
